from django.shortcuts import render
from django.http import HttpResponse
import requests
import json

# Create your views here.
def index(request):
    data = {}
    return render(request, "books.html", data)

def books(request):
    response = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting")
    books_data = response.json()
    books_data = {"books": books_data["items"]}
    books_json_data = json.dumps(books_data)
    return HttpResponse(books_json_data, content_type="application/json")

def search(request, name):
    response = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + name)
    books_data = response.json()
    books_data = {"books": books_data["items"]}
    books_json_data = json.dumps(books_data)
    return HttpResponse(books_json_data, content_type="application/json")

    
