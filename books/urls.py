from django.urls import path
from . import views

app_name = 'books'

urlpatterns = [
    path('', views.index),
    path('api/', views.books),
    path('api/<str:name>', views.search),
]

