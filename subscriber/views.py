from django.http import JsonResponse
from django.shortcuts import render
from .forms import SubscriberForm
from .models import Subscriber

# Create your views here.
def index(request):
    form = SubscriberForm(request.POST)
    if request.method == "POST" and form.is_valid():
        data = form.cleaned_data
        status_subscribe = True
        try:
            Subscriber.objects.create(**data)
        except:
            status_subscribe = False
        return JsonResponse({'status_subscribe': status_subscribe})
    return render(request, 'subscriber.html', {'form': form})

def validate(request):
    if request.method == "POST":
        exists = Subscriber.objects.filter(email = request.POST['email']).first()
        return JsonResponse({'exists': exists})
        

def subscriber(request):
    if request.method == "POST":
        subscriber = Subscriber.objects.all().values()
        subscriberList = list(subscriber)
        return JsonResponse({'subscriberList': subscriberList})

def result(request):
    return render(request, 'subscriber-data.html')

def unsubscribe(request):
    if request.method == "POST":
        Subscriber.objects.filter(email = request.POST['email']).delete()
        now = list(Subscriber.objects.all().values())
        return JsonResponse({'unsubscribed': True, 'now': now})