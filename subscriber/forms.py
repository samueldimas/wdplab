from django import forms

class SubscriberForm(forms.Form):
    name = forms.CharField(widget = forms.TextInput(attrs = {
        'id': 'name',
        'placeholder': 'Name',
        'required': True,
    })) 
    email = forms.EmailField(widget = forms.EmailInput(attrs = {
        'id': 'email',
        'placeholder': 'Email',
        'required': True,
    }))
    password = forms.CharField(widget = forms.PasswordInput(attrs= {
        'id': 'password',
        'placeholder': 'Password',
        'required': True,
    }))