from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name = 'index'),
    path('validate/', views.validate, name="validate"),
    path('subscriber/', views.subscriber, name="subscriber"),
    path('result/', views.result, name="result"),
    path('unsubscribe/', views.unsubscribe, name='unsubscribe'),

]