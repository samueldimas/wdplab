function renderResult() {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();

    $.ajax({
        method: 'POST',
        url: 'http://localhost:8000/subscribe/subscriber/',
        headers: {
            "X-CSRFToken": csrftoken
        },
        success: function (result) {
            var length_arr = result.listDataSubscribe.length;
            var data = result.listDataSubscribe;
            for (var i = 0; i < length_arr; i++) {
                var name = data[i]['nama'];
                var email = data[i]['email'];
                var button = '<button class="btn" id=' + email + ' onclick=unsubscribe(this)>' + 'UNSUBSCRIBE' + '</button>';
                var show = '<div class = "hasil">' + name + " " + email + button + '</div>' ;
                var container = '<div>' + show + '</div>';
                $('.result').append(container)
            }
        }
    })
}

function unsubscribe(t) {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var email = t.id;
    $.ajax({
        method: 'POST',
        url: "{% url 'unsubscribe' %}",
        headers: {
            "X-CSRFToken": csrftoken
        },
        data: {email: email},
        success: function (res) {
            console.log('success');
            $('#res').replaceWith("<div id='res' class='result'></div>");
            var length_arr = res.now.length;
            var data = res.now;
            for (var i = 0; i < length_arr; i++) {
                var name = data[i]['nama'];
                var email = data[i]['email'];
                var button = '<button class="btn" id=' + email + ' onclick=unsubscribe(this)>' + 'UNSUBSCRIBE' + '</button>';
                var show = '<div class = "hasil">' + name + " " + email + button + '</div>' ;
                $('.result').append(show)
            }
        }
    })
}
