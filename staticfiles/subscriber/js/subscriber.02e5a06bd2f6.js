function checkValid() {
    var email = $("#email").val();
    var token = $('input[name="csrfmiddlewaretoken"]').attr('value')

    $.ajax({
        method: "POST",
        url: "http://localhost:8000/subscribe/validate",
        data : JSON.stringify({'email': email, 'name': name, 'password': password}),
        beforeSend: function(request) {
            request.setRequestHeader('X-CSRFToken', token)
        },
        success: function(response) {
            var exist = response.existingEmail;
            var valid = response.validForm;
            if (exist || !valid) {
                console.log("masuk");
                $("#btn-submit").prop(disabled, true);
            } else {
                console.log("bisa");
                $('#btn-submit').prop(disabled, false)
            }
        },
    })
}

function postForm() {
    event.preventDefault(true);
    var email = $("#email").val();
    var name = $("#name").val();
    var password = $("#password").val();
    var token = $('input[name="csrfmiddlewaretoken"]').attr('value')

    $.ajax({
        method: 'POST',
        url: 'http://localhost:8000/subscribe/submit',
        data: JSON.stringify({'email': email, 'name': name, 'password': password}),
        beforeSend: function(request) {
            request.setRequestHeader("X-CSRFToken", token);
        },
    })

}