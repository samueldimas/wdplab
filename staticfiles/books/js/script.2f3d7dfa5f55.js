function getData(url, name) {
    $.ajax({    
        url: url + name,
        dataType: "json",
        success: function(data) {
            let dataString = "";
            $.each(data.books, function(key, value) {
                dataString += "<tr>";
                dataString += "<td>" + value.volumeInfo.title + "</td>";
                dataString += "<td>" + value.volumeInfo.authors + "</td>";
                dataString += "<td>" + value.volumeInfo.publisher + "</td>";
                dataString += "<td>" + value.volumeInfo.publishedDate + "</td>";
                dataString += "<td><button class='favor-button'></button></td>";
                dataString += "</tr>";
            });
            $("#books").append(dataString);
        }
    });
  }
  
  $("document").ready(function() {
    getData("http://samueldimas.herokuapp.com/books/api/", "quilting");
    $("table").on("click", ".favor-button", function() {
        if ($(this).attr("class").split(" ").length == 1) {
            $(this).addClass("marked-favor-button");
            $(".counter-number").text(eval($(".counter-number").text()) + 1);
        } else {
            $(this).removeClass("marked-favor-button");
            $(".counter-number").text(eval($(".counter-number").text()) - 1);
        }
    });
    
    $("#search-button").on("click", function() {
        $(".counter-number").text(0);
        $("#books").remove();
        $("table").append("<tbody id='books'></tbody>");
        let name = $(".search-input").val();
        getData("http://samueldimas.herokuapp.com/books/api/", name);
    });
  });
  