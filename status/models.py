from django.db import models

# Create your models here.
class Status(models.Model):
    text = models.CharField(max_length=300)
    time = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Statuses'