from django.shortcuts import render, redirect
from . import forms
from .models import Status

# Create your views here.
def index(request):
    statuses = Status.objects.all()
    status_form = forms.StatusForm()
    response = {"statuses" : statuses, "status_form" : status_form}
    return render(request, 'index.html', response)

def add_status(request):
    status_form = forms.StatusForm(request.POST)
    if request.method == "POST" and status_form.is_valid():
        Status.objects.create(text = status_form.cleaned_data['text'])
    return redirect('status:index')

def profile(request):
    return render(request, 'profile.html')
