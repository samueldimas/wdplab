from django import forms

class StatusForm(forms.Form):
    text = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': "Enter your status here",
        'required': True,
    }), max_length=300)