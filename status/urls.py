from django.urls import path
from . import views

app_name = "status"

urlpatterns = [
    path('', views.index, name="index"),
    path('add_status', views.add_status, name='add_status'),
]
