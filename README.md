# Lab WDP Class B - Samuel Dimas Partogi
This Gitlab repository is the result of the work from Samuel Dimas Partogi -  WDP Class B

## Pipeline and Coverage
[![pipeline status](https://gitlab.com/samueldimas/wdplab/badges/master/pipeline.svg)](https://gitlab.com/samueldimas/wdplab/commits/master)
[![coverage report](https://gitlab.com/samueldimas/wdplab/badges/master/coverage.svg)](https://gitlab.com/samueldimas/wdplab/commits/master)

## Heroku
Click [here](http://samueldimas.herokuapp.com) to see my website.

## Project Member
- Samuel Dimas Partogi - 1706074915

## Acknowledgements
- Course: WDP 2018 Gasal
- Lecturer: Ibu Maya Retno Ayu Setyautami S.Kom., M.Kom.
- Class B Teaching Assistant